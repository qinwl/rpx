function Rpx (options) {
  this.config = {
    "rate": 100, // 字体大小， 默认 1rem = 100px
    "designWidth": 750, //设计稿的宽度
    "maxWidth": -1, // 设置支持缩放的最大宽度
    "minWidth": -1 // 设置支持缩放的最小宽度
  }
  Object.assign(this.config, options)
  this.calc()
  this.onresize()
}

Rpx.prototype = {
  calc: function () {
    var html = document.getElementsByTagName('html')[0]
    var deviceWidth = html.offsetWidth
    if (this.config.maxWidth !== -1)
      deviceWidth = deviceWidth > this.config.maxWidth ? this.config.maxWidth : deviceWidth
    if (this.config.minWidth !== -1)
      deviceWidth = deviceWidth < this.config.minWidth ? this.config.minWidth : deviceWidth
    var fontSize = this.config.rate * deviceWidth / this.config.designWidth
    html.style.fontSize = fontSize + 'px'
    console.log(fontSize)
  },
  onresize: function () {
    self = this
    window.onresize = function () {
      self.calc()
    }
  }
}